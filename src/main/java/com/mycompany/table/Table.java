package com.mycompany.table;

public class Table {

    final int row = 3, col = 3;
    String table[][] = new String[row][col]; 
    
    public void Welcome(){
        System.out.println("Welcome to OX");
    }
    
    public void gameOver(){
        System.out.println("Game Over!!");
    }
    
    //create default table
    public void startTable(){
        for(int i=0; i<row; i++){
            for(int j=0; j<col; j++){
                    table[i][j] = "- ";
                    System.out.print(table[i][j]);
                }
                System.out.println();
            }
    }
}
