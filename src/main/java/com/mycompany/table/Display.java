package com.mycompany.table;

import java.util.Scanner;

public class Display extends Table{
    public int row;
    public int col;
    public String player;
    
    Scanner kb = new Scanner(System.in);
    
    public Display(){
        Welcome();
        startTable();
    }
    public void Turn(String player){
        System.out.println("Turn " + player);
        System.out.print("Please input row , col : ");
    }
    public void tablePlayer(int row, int col, String player){
        this.row = row;
        this.col = col;
        
        for(int i=0; i<3; i++){
                for(int j=0; j<3; j++){
                    table[row][col] = player;
                    if(i == row && j == col){
                        System.out.print(table[row][col]); //x or o in table
                    }else{
                        System.out.print(table[i][j]); // print - 
                    }   
                }
                System.out.println();
            }
    }

    public void check(String player){ //check for win
        //check row
        for(int r = 0; r<3; r++){
            if(table[r][0].equals(player) && table[r][1].equals(player) && table[r][2].equals(player)){ //checkrow
                win(player);
                askquestion();
                break;
            }else if(table[0][r].equals(player) && table[1][r].equals(player) && table[2][r].equals(player)){ //checkcol
                win(player);
                askquestion();
                break;
            }else if(table[0][0].equals(player) && table[1][1].equals(player) && table[2][2].equals(player)){ 
                win(player);
                askquestion();
                break;
            }else if(table[0][2].equals(player)&& table[1][1].equals(player)&& table[2][0].equals(player)){
                win(player);
                askquestion();
                break;
            }
        }
    }
    
    public void repeatrowcol(String player){ //check input row col
        int In_row;
        int In_col;
        int count = 0;
        Turn(player);
        while(count == 0){
            In_row = kb.nextInt()-1;
            In_col = kb.nextInt()-1;
            if(table[In_row][In_col].equals("- ")){
                tablePlayer(In_row, In_col, player);
                check(player);
                count++; 
            }else{
                System.out.print("Please input row, col again: ");
            }
        }
    }
    public void playGame(){
        for(int n = 0; n<9; n++){
            if(n%2 == 0){
                repeatrowcol("X "); //for check row and col
            }else{
                repeatrowcol("O ");
            }if(n == 8){
                System.out.println("Draw");
                askquestion();
            }
        }
    }
    public void askquestion(){
        gameOver();
        System.out.print("Continue? [y/n] : ");
        String ans = kb.next();
        if(ans.equals("y")){
            startTable();
            playGame();
        }else if(ans.equals("n")){
            
            System.exit(0);
        }
    }
    public void win(String player){
        System.out.println(player + " win!!");
    }
    
}
    
